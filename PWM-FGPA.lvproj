﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO-9056-01DEC88F" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO-9056-01DEC88F</Property>
		<Property Name="alias.value" Type="Str">192.168.4.158</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,Linux;CPU,x64;DeviceCode,79DF;</Property>
		<Property Name="crio.ControllerPID" Type="Str">79DF</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">9</Property>
		<Property Name="host.TargetOSID" Type="UInt">19</Property>
		<Property Name="host.TargetUIEnabled" Type="Bool">false</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">false</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="CalculateFrequency.vi" Type="VI" URL="../VI´s/CalculateFrequency.vi"/>
		<Item Name="CalculateTime.vi" Type="VI" URL="../VI´s/CalculateTime.vi"/>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9056</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
			</Item>
			<Item Name="Real-Time Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.DAQModuleContainer</Property>
			</Item>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{06814974-D3CD-4597-9095-079753209D5B}resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{1268E102-78AD-4A41-8404-5707CA1DA2B0}resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{16095B4D-8457-4151-B133-C880CB1940EA}resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{19E1BA04-C2F0-463C-BA37-645C212D0871}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{1A945678-B44B-4E0C-87D4-58C7409AD85F}resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{1D5D03DC-3C64-464B-A0EB-5AC40276979D}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{1E3E8B98-B26E-4255-95EF-4B475E670B8B}resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}resource=/Reset RT App;0;WriteMethodType=bool{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{38084E7E-68F1-4B96-BA0C-983C2D2FF785}resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{3D84ED28-1331-411E-BA63-69961CFA3319}resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{42831A25-F2FA-43D0-ABD9-E438F12672F1}resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}resource=/Scan Clock;0;ReadMethodType=bool{4DC40A22-FEF3-442E-9B29-56008BB23AAF}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{59EFED26-1EC8-4468-8F9D-E31371094949}resource=/Chassis Temperature;0;ReadMethodType=i16{5B19EAC7-7141-495B-BD68-C7DD71E64961}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{6A742C6D-245F-4BD5-8412-42B1F280D684}resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{7613280B-7D12-486C-BE47-B014B1379BC5}resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"{7B581DF8-6C7B-4072-AEE0-841DE51E0167}resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{80631394-0766-45A8-9C45-85A416163672}resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{8E4A6818-798B-4018-AC29-131D1775903C}resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{92BA4316-B965-472E-AED5-B6BE705DC2D3}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{98172291-D5C2-4D7B-9614-A7DE890CDBA5}resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{9CED241E-22C0-4EDB-9108-E9F146084316}resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{A703296C-D6F2-4CC0-8320-7AB736B5B363}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{A81E7E41-796A-475F-9F54-1642F5C28C3A}resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B3053B94-8A27-4D5B-BA5A-07E34E977E22}resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{B78AAE80-7EB9-4A99-8C96-FF69623D366C}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{B917F18D-F269-403B-881E-A865ACDE8947}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{BC0BF488-BB99-44EB-898D-0C8FB46F761E}resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C8D1A4AD-21B2-4592-9704-823610A23930}resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{D128D618-7BD3-40E7-A176-623CCD8DCFD6}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{D34C9492-534D-411B-8A4A-4DF0CEB572E2}resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{D5074809-4633-4FF1-AB39-38C4F195CCA2}resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{D750A39B-FE95-4BC8-A397-827852FE38B2}resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{E9602944-28FC-4EEF-B141-1CC1A6CA5672}resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{F53E55B5-9739-4286-8BBD-2269A02C13BA}resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{FFCCC479-F681-476A-AAA3-0D7D692C79D4}resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16Counters"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod8/DIO0resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO10resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO11resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO12resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO13resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO14resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO15:8resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO15resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO16resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO17resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO18resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO19resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO1resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO20resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO21resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO22resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO23:16resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO23resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO24resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO25resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO26resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO27resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO28resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO29resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO2resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO30resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO31:0resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod8/DIO31:24resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO31resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO3resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO4resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO5resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO6resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO7:0resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO7resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO8resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO9resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod8[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolValues"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9056</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{59EFED26-1EC8-4468-8F9D-E31371094949}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}</Property>
					</Item>
					<Item Name="Reset RT App" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Reset RT App</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}</Property>
					</Item>
					<Item Name="System Watchdog Expired" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/System Watchdog Expired</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B78AAE80-7EB9-4A99-8C96-FF69623D366C}</Property>
					</Item>
					<Item Name="12.8 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/12.8 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}</Property>
					</Item>
					<Item Name="13.1072 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/13.1072 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}</Property>
					</Item>
					<Item Name="10 MHz Timebase" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/10 MHz Timebase</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{19E1BA04-C2F0-463C-BA37-645C212D0871}</Property>
					</Item>
					<Item Name="USER FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/USER FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B917F18D-F269-403B-881E-A865ACDE8947}</Property>
					</Item>
				</Item>
				<Item Name="cRIO_Trig" Type="Folder">
					<Item Name="cRIO_Trig0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{92BA4316-B965-472E-AED5-B6BE705DC2D3}</Property>
					</Item>
					<Item Name="cRIO_Trig1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}</Property>
					</Item>
					<Item Name="cRIO_Trig2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}</Property>
					</Item>
					<Item Name="cRIO_Trig3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}</Property>
					</Item>
					<Item Name="cRIO_Trig4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}</Property>
					</Item>
					<Item Name="cRIO_Trig5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}</Property>
					</Item>
					<Item Name="cRIO_Trig6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4DC40A22-FEF3-442E-9B29-56008BB23AAF}</Property>
					</Item>
					<Item Name="cRIO_Trig7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/cRIO_Trig/cRIO_Trig7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D128D618-7BD3-40E7-A176-623CCD8DCFD6}</Property>
					</Item>
				</Item>
				<Item Name="Time Synchronization" Type="Folder">
					<Item Name="Time" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A703296C-D6F2-4CC0-8320-7AB736B5B363}</Property>
					</Item>
					<Item Name="Time Source" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time Source</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1D5D03DC-3C64-464B-A0EB-5AC40276979D}</Property>
					</Item>
					<Item Name="Time Synchronization Fault" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Time Synchronization Fault</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}</Property>
					</Item>
					<Item Name="Offset from Time Reference" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Offset from Time Reference</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}</Property>
					</Item>
					<Item Name="Offset from Time Reference Valid" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>0</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/Time Synchronization/Offset from Time Reference Valid</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}</Property>
					</Item>
				</Item>
				<Item Name="Mod8" Type="Folder">
					<Item Name="Mod8/DIO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D34C9492-534D-411B-8A4A-4DF0CEB572E2}</Property>
					</Item>
					<Item Name="Mod8/DIO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3D84ED28-1331-411E-BA63-69961CFA3319}</Property>
					</Item>
					<Item Name="Mod8/DIO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7B581DF8-6C7B-4072-AEE0-841DE51E0167}</Property>
					</Item>
					<Item Name="Mod8/DIO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{38084E7E-68F1-4B96-BA0C-983C2D2FF785}</Property>
					</Item>
					<Item Name="Mod8/DIO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{80631394-0766-45A8-9C45-85A416163672}</Property>
					</Item>
					<Item Name="Mod8/DIO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}</Property>
					</Item>
					<Item Name="Mod8/DIO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1268E102-78AD-4A41-8404-5707CA1DA2B0}</Property>
					</Item>
					<Item Name="Mod8/DIO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}</Property>
					</Item>
					<Item Name="Mod8/DIO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3053B94-8A27-4D5B-BA5A-07E34E977E22}</Property>
					</Item>
					<Item Name="Mod8/DIO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{42831A25-F2FA-43D0-ABD9-E438F12672F1}</Property>
					</Item>
					<Item Name="Mod8/DIO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BC0BF488-BB99-44EB-898D-0C8FB46F761E}</Property>
					</Item>
					<Item Name="Mod8/DIO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FFCCC479-F681-476A-AAA3-0D7D692C79D4}</Property>
					</Item>
					<Item Name="Mod8/DIO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D750A39B-FE95-4BC8-A397-827852FE38B2}</Property>
					</Item>
					<Item Name="Mod8/DIO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}</Property>
					</Item>
					<Item Name="Mod8/DIO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}</Property>
					</Item>
					<Item Name="Mod8/DIO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F53E55B5-9739-4286-8BBD-2269A02C13BA}</Property>
					</Item>
					<Item Name="Mod8/DIO16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}</Property>
					</Item>
					<Item Name="Mod8/DIO17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{16095B4D-8457-4151-B133-C880CB1940EA}</Property>
					</Item>
					<Item Name="Mod8/DIO18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A81E7E41-796A-475F-9F54-1642F5C28C3A}</Property>
					</Item>
					<Item Name="Mod8/DIO19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{06814974-D3CD-4597-9095-079753209D5B}</Property>
					</Item>
					<Item Name="Mod8/DIO20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C8D1A4AD-21B2-4592-9704-823610A23930}</Property>
					</Item>
					<Item Name="Mod8/DIO21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}</Property>
					</Item>
					<Item Name="Mod8/DIO22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}</Property>
					</Item>
					<Item Name="Mod8/DIO23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9CED241E-22C0-4EDB-9108-E9F146084316}</Property>
					</Item>
					<Item Name="Mod8/DIO24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8E4A6818-798B-4018-AC29-131D1775903C}</Property>
					</Item>
					<Item Name="Mod8/DIO25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6A742C6D-245F-4BD5-8412-42B1F280D684}</Property>
					</Item>
					<Item Name="Mod8/DIO26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D5074809-4633-4FF1-AB39-38C4F195CCA2}</Property>
					</Item>
					<Item Name="Mod8/DIO27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1A945678-B44B-4E0C-87D4-58C7409AD85F}</Property>
					</Item>
					<Item Name="Mod8/DIO28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7613280B-7D12-486C-BE47-B014B1379BC5}</Property>
					</Item>
					<Item Name="Mod8/DIO29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1E3E8B98-B26E-4255-95EF-4B475E670B8B}</Property>
					</Item>
					<Item Name="Mod8/DIO30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{98172291-D5C2-4D7B-9614-A7DE890CDBA5}</Property>
					</Item>
					<Item Name="Mod8/DIO31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}</Property>
					</Item>
					<Item Name="Mod8/DIO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}</Property>
					</Item>
					<Item Name="Mod8/DIO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E9602944-28FC-4EEF-B141-1CC1A6CA5672}</Property>
					</Item>
					<Item Name="Mod8/DIO23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}</Property>
					</Item>
					<Item Name="Mod8/DIO31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}</Property>
					</Item>
					<Item Name="Mod8/DIO31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod8/DIO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="Mod8" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 8</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">00000000000000000000000000000000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{5B19EAC7-7141-495B-BD68-C7DD71E64961}</Property>
				</Item>
				<Item Name="FPGA.vi" Type="VI" URL="../FPGA/FPGA.vi">
					<Property Name="configString.guid" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{06814974-D3CD-4597-9095-079753209D5B}resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{1268E102-78AD-4A41-8404-5707CA1DA2B0}resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{16095B4D-8457-4151-B133-C880CB1940EA}resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{19E1BA04-C2F0-463C-BA37-645C212D0871}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{1A945678-B44B-4E0C-87D4-58C7409AD85F}resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{1D5D03DC-3C64-464B-A0EB-5AC40276979D}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{1E3E8B98-B26E-4255-95EF-4B475E670B8B}resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}resource=/Reset RT App;0;WriteMethodType=bool{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{38084E7E-68F1-4B96-BA0C-983C2D2FF785}resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{3D84ED28-1331-411E-BA63-69961CFA3319}resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{42831A25-F2FA-43D0-ABD9-E438F12672F1}resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}resource=/Scan Clock;0;ReadMethodType=bool{4DC40A22-FEF3-442E-9B29-56008BB23AAF}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{59EFED26-1EC8-4468-8F9D-E31371094949}resource=/Chassis Temperature;0;ReadMethodType=i16{5B19EAC7-7141-495B-BD68-C7DD71E64961}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{6A742C6D-245F-4BD5-8412-42B1F280D684}resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{7613280B-7D12-486C-BE47-B014B1379BC5}resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"{7B581DF8-6C7B-4072-AEE0-841DE51E0167}resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{80631394-0766-45A8-9C45-85A416163672}resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{8E4A6818-798B-4018-AC29-131D1775903C}resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{92BA4316-B965-472E-AED5-B6BE705DC2D3}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{98172291-D5C2-4D7B-9614-A7DE890CDBA5}resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{9CED241E-22C0-4EDB-9108-E9F146084316}resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{A703296C-D6F2-4CC0-8320-7AB736B5B363}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{A81E7E41-796A-475F-9F54-1642F5C28C3A}resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B3053B94-8A27-4D5B-BA5A-07E34E977E22}resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{B78AAE80-7EB9-4A99-8C96-FF69623D366C}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{B917F18D-F269-403B-881E-A865ACDE8947}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{BC0BF488-BB99-44EB-898D-0C8FB46F761E}resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C8D1A4AD-21B2-4592-9704-823610A23930}resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{D128D618-7BD3-40E7-A176-623CCD8DCFD6}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{D34C9492-534D-411B-8A4A-4DF0CEB572E2}resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{D5074809-4633-4FF1-AB39-38C4F195CCA2}resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{D750A39B-FE95-4BC8-A397-827852FE38B2}resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{E9602944-28FC-4EEF-B141-1CC1A6CA5672}resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{F53E55B5-9739-4286-8BBD-2269A02C13BA}resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{FFCCC479-F681-476A-AAA3-0D7D692C79D4}resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16Counters"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod8/DIO0resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO10resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO11resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO12resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO13resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO14resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO15:8resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO15resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO16resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO17resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO18resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO19resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO1resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO20resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO21resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO22resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO23:16resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO23resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO24resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO25resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO26resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO27resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO28resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO29resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO2resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO30resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO31:0resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod8/DIO31:24resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO31resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO3resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO4resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO5resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO6resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO7:0resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO7resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO8resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO9resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod8[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolValues"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\Users\m.schmidt\Desktop\Projekte\FPGA Bitfiles\PWM-FGPA_FPGATarget_FPGA_noLuArdA6IE.lvbitx</Property>
				</Item>
				<Item Name="Saved.ctl" Type="VI" URL="../TypeDefs/Saved.ctl">
					<Property Name="configString.guid" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{06814974-D3CD-4597-9095-079753209D5B}resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{1268E102-78AD-4A41-8404-5707CA1DA2B0}resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{16095B4D-8457-4151-B133-C880CB1940EA}resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{19E1BA04-C2F0-463C-BA37-645C212D0871}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{1A945678-B44B-4E0C-87D4-58C7409AD85F}resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{1D5D03DC-3C64-464B-A0EB-5AC40276979D}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{1E3E8B98-B26E-4255-95EF-4B475E670B8B}resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}resource=/Reset RT App;0;WriteMethodType=bool{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{38084E7E-68F1-4B96-BA0C-983C2D2FF785}resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{3D84ED28-1331-411E-BA63-69961CFA3319}resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{42831A25-F2FA-43D0-ABD9-E438F12672F1}resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}resource=/Scan Clock;0;ReadMethodType=bool{4DC40A22-FEF3-442E-9B29-56008BB23AAF}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{59EFED26-1EC8-4468-8F9D-E31371094949}resource=/Chassis Temperature;0;ReadMethodType=i16{5B19EAC7-7141-495B-BD68-C7DD71E64961}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{6A742C6D-245F-4BD5-8412-42B1F280D684}resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{7613280B-7D12-486C-BE47-B014B1379BC5}resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"{7B581DF8-6C7B-4072-AEE0-841DE51E0167}resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{80631394-0766-45A8-9C45-85A416163672}resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{8E4A6818-798B-4018-AC29-131D1775903C}resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{92BA4316-B965-472E-AED5-B6BE705DC2D3}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{98172291-D5C2-4D7B-9614-A7DE890CDBA5}resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{9CED241E-22C0-4EDB-9108-E9F146084316}resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{A703296C-D6F2-4CC0-8320-7AB736B5B363}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{A81E7E41-796A-475F-9F54-1642F5C28C3A}resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B3053B94-8A27-4D5B-BA5A-07E34E977E22}resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{B78AAE80-7EB9-4A99-8C96-FF69623D366C}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{B917F18D-F269-403B-881E-A865ACDE8947}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{BC0BF488-BB99-44EB-898D-0C8FB46F761E}resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C8D1A4AD-21B2-4592-9704-823610A23930}resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{D128D618-7BD3-40E7-A176-623CCD8DCFD6}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{D34C9492-534D-411B-8A4A-4DF0CEB572E2}resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{D5074809-4633-4FF1-AB39-38C4F195CCA2}resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{D750A39B-FE95-4BC8-A397-827852FE38B2}resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{E9602944-28FC-4EEF-B141-1CC1A6CA5672}resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{F53E55B5-9739-4286-8BBD-2269A02C13BA}resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{FFCCC479-F681-476A-AAA3-0D7D692C79D4}resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16Counters"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod8/DIO0resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO10resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO11resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO12resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO13resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO14resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO15:8resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO15resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO16resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO17resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO18resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO19resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO1resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO20resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO21resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO22resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO23:16resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO23resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO24resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO25resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO26resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO27resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO28resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO29resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO2resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO30resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO31:0resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod8/DIO31:24resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO31resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO3resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO4resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO5resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO6resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO7:0resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO7resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO8resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO9resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod8[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolValues"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				</Item>
				<Item Name="Values" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">1023</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">0</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">2</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">1023</Property>
					<Property Name="Type" Type="UInt">2</Property>
					<Property Name="Type Descriptor" Type="Str">1000800000000001000A402104426F6F6C00000100000000000000</Property>
				</Item>
				<Item Name="Counters" Type="FPGA FIFO">
					<Property Name="Actual Number of Elements" Type="UInt">1023</Property>
					<Property Name="Arbitration for Read" Type="UInt">1</Property>
					<Property Name="Arbitration for Write" Type="UInt">1</Property>
					<Property Name="Control Logic" Type="UInt">0</Property>
					<Property Name="Data Type" Type="UInt">3</Property>
					<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
					<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
					<Property Name="fifo.configured" Type="Bool">true</Property>
					<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
					<Property Name="fifo.valid" Type="Bool">true</Property>
					<Property Name="fifo.version" Type="Int">12</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}</Property>
					<Property Name="Local" Type="Bool">false</Property>
					<Property Name="Memory Type" Type="UInt">2</Property>
					<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
					<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
					<Property Name="Requested Number of Elements" Type="UInt">1023</Property>
					<Property Name="Type" Type="UInt">2</Property>
					<Property Name="Type Descriptor" Type="Str">1000800000000001000940030003493332000100000000000000000000</Property>
				</Item>
				<Item Name="FPGAState.ctl" Type="VI" URL="../TypeDefs/FPGAState.ctl">
					<Property Name="configString.guid" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{06814974-D3CD-4597-9095-079753209D5B}resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{1268E102-78AD-4A41-8404-5707CA1DA2B0}resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{16095B4D-8457-4151-B133-C880CB1940EA}resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{19E1BA04-C2F0-463C-BA37-645C212D0871}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{1A945678-B44B-4E0C-87D4-58C7409AD85F}resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{1D5D03DC-3C64-464B-A0EB-5AC40276979D}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{1E3E8B98-B26E-4255-95EF-4B475E670B8B}resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}resource=/Reset RT App;0;WriteMethodType=bool{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{38084E7E-68F1-4B96-BA0C-983C2D2FF785}resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{3D84ED28-1331-411E-BA63-69961CFA3319}resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{42831A25-F2FA-43D0-ABD9-E438F12672F1}resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}resource=/Scan Clock;0;ReadMethodType=bool{4DC40A22-FEF3-442E-9B29-56008BB23AAF}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{59EFED26-1EC8-4468-8F9D-E31371094949}resource=/Chassis Temperature;0;ReadMethodType=i16{5B19EAC7-7141-495B-BD68-C7DD71E64961}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{6A742C6D-245F-4BD5-8412-42B1F280D684}resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{7613280B-7D12-486C-BE47-B014B1379BC5}resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"{7B581DF8-6C7B-4072-AEE0-841DE51E0167}resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{80631394-0766-45A8-9C45-85A416163672}resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{8E4A6818-798B-4018-AC29-131D1775903C}resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{92BA4316-B965-472E-AED5-B6BE705DC2D3}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{98172291-D5C2-4D7B-9614-A7DE890CDBA5}resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{9CED241E-22C0-4EDB-9108-E9F146084316}resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{A703296C-D6F2-4CC0-8320-7AB736B5B363}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{A81E7E41-796A-475F-9F54-1642F5C28C3A}resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B3053B94-8A27-4D5B-BA5A-07E34E977E22}resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{B78AAE80-7EB9-4A99-8C96-FF69623D366C}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{B917F18D-F269-403B-881E-A865ACDE8947}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{BC0BF488-BB99-44EB-898D-0C8FB46F761E}resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C8D1A4AD-21B2-4592-9704-823610A23930}resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{D128D618-7BD3-40E7-A176-623CCD8DCFD6}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{D34C9492-534D-411B-8A4A-4DF0CEB572E2}resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{D5074809-4633-4FF1-AB39-38C4F195CCA2}resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{D750A39B-FE95-4BC8-A397-827852FE38B2}resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{E9602944-28FC-4EEF-B141-1CC1A6CA5672}resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{F53E55B5-9739-4286-8BBD-2269A02C13BA}resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{FFCCC479-F681-476A-AAA3-0D7D692C79D4}resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16Counters"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod8/DIO0resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO10resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO11resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO12resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO13resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO14resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO15:8resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO15resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO16resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO17resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO18resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO19resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO1resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO20resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO21resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO22resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO23:16resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO23resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO24resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO25resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO26resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO27resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO28resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO29resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO2resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO30resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO31:0resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod8/DIO31:24resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO31resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO3resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO4resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO5resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO6resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO7:0resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO7resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO8resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO9resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod8[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolValues"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				</Item>
				<Item Name="FPGAProcData.ctl" Type="VI" URL="../TypeDefs/FPGAProcData.ctl">
					<Property Name="configString.guid" Type="Str">{04B17001-384B-4C56-A347-C73305B136A5}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=bool{053D9E13-BDF1-4D4E-B86E-D113F5B87C5B}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=bool{05D1EBBD-F31C-448E-9FC7-9C43C41AB024}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=bool{06814974-D3CD-4597-9095-079753209D5B}resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{08FB1014-EE5D-4E11-A21D-B082FBD16B7D}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=bool{1268E102-78AD-4A41-8404-5707CA1DA2B0}resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{16095B4D-8457-4151-B133-C880CB1940EA}resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{19E1BA04-C2F0-463C-BA37-645C212D0871}NumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool{1A945678-B44B-4E0C-87D4-58C7409AD85F}resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{1D22653D-6ABD-4D8A-BFDF-ACB9A5BC1578}resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{1D5D03DC-3C64-464B-A0EB-5AC40276979D}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctl{1E3E8B98-B26E-4255-95EF-4B475E670B8B}resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{2AA09AC8-FECF-462D-9DAE-D9D67F324FBB}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"{2AD31F87-85A4-4B23-94E8-3B6BB7D5D14B}resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{2C0C879A-1A96-41FA-BBDB-F4ACF34BECC1}resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{2D981AAD-3C54-4F4A-BAC7-A6876EA63B0D}resource=/Reset RT App;0;WriteMethodType=bool{2F8D299E-6E30-442D-932E-7E0D3AC6D5C9}resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{38084E7E-68F1-4B96-BA0C-983C2D2FF785}resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{3D84ED28-1331-411E-BA63-69961CFA3319}resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{4164B496-5F5F-4555-ABD8-D9DDDAAF8BFF}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=bool{42831A25-F2FA-43D0-ABD9-E438F12672F1}resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{4322DC41-4C3E-4C80-B0FE-0AF5860A1C7B}resource=/Scan Clock;0;ReadMethodType=bool{4DC40A22-FEF3-442E-9B29-56008BB23AAF}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=bool{4E86C0D5-D9FB-45D9-BFA7-E9A6436A782F}resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{4EBA9529-8CBD-40FB-9F43-A7927B7F6858}resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{58EF261D-3BC2-4E23-9112-AC9F16EFC72D}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{59EFED26-1EC8-4468-8F9D-E31371094949}resource=/Chassis Temperature;0;ReadMethodType=i16{5B19EAC7-7141-495B-BD68-C7DD71E64961}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{6A742C6D-245F-4BD5-8412-42B1F280D684}resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{6D42F1C1-26E8-480A-B787-4A80F77D0A7D}resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{7613280B-7D12-486C-BE47-B014B1379BC5}resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{763E9FB5-FCB6-4D2A-A529-A507F22CE3EE}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"{7B581DF8-6C7B-4072-AEE0-841DE51E0167}resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{80631394-0766-45A8-9C45-85A416163672}resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{8B90B147-F7E3-4228-98D2-9DE86B0BCF35}NumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool{8E4A6818-798B-4018-AC29-131D1775903C}resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{90E6815E-FE1F-4F45-9D23-68CEE47FD4EB}resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{92BA4316-B965-472E-AED5-B6BE705DC2D3}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=bool{961F3595-63E1-4E03-A698-EC4FEE6BDB7A}resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{98172291-D5C2-4D7B-9614-A7DE890CDBA5}resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{9CED241E-22C0-4EDB-9108-E9F146084316}resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{9F7880FF-B15B-4C43-A5EC-2DAEE015DC0D}resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{A3F2D9F9-5545-4E7E-A2BA-58CACBB547C3}NumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool{A703296C-D6F2-4CC0-8320-7AB736B5B363}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64{A81E7E41-796A-475F-9F54-1642F5C28C3A}resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{AEBC25E4-BD0B-45CD-BFDD-92DB77E5D6EB}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=bool{AFFD1D4D-C4A8-4B8E-9182-CF80DA583E02}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{B09B68C0-9BC1-466F-88B8-FA2B6691C2B7}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{B3053B94-8A27-4D5B-BA5A-07E34E977E22}resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{B78AAE80-7EB9-4A99-8C96-FF69623D366C}NumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=bool{B917F18D-F269-403B-881E-A865ACDE8947}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{BC0BF488-BB99-44EB-898D-0C8FB46F761E}resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{C8D1A4AD-21B2-4592-9704-823610A23930}resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{D128D618-7BD3-40E7-A176-623CCD8DCFD6}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=bool{D34C9492-534D-411B-8A4A-4DF0CEB572E2}resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{D5074809-4633-4FF1-AB39-38C4F195CCA2}resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{D750A39B-FE95-4BC8-A397-827852FE38B2}resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{DAB50B70-95C8-439F-BA0A-E5EAD6C53BF1}resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{E427CA44-36CE-4B18-B15E-5C0FE328B4EE}NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=bool{E9602944-28FC-4EEF-B141-1CC1A6CA5672}resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{EA00B7D5-F27F-4DE7-ABC0-3172458D0310}resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{F53E55B5-9739-4286-8BBD-2269A02C13BA}resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{FD3F8CAF-6876-4AED-BF09-96DCDBB649A8}NumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32{FFCCC479-F681-476A-AAA3-0D7D692C79D4}resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">10 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/10 MHz Timebase;0;ReadMethodType=bool12.8 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/12.8 MHz Timebase;0;ReadMethodType=bool13.1072 MHz TimebaseNumberOfSyncRegistersForReadInProject=Auto;resource=/13.1072 MHz Timebase;0;ReadMethodType=bool40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16Counters"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Counters;DataType=1000800000000001000940030003493332000100000000000000000000;DisableOnOverflowUnderflow=FALSE"cRIO_Trig0ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig0;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig1ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig1;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig2ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig2;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig3ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig3;0;ReadMethodType=bool;WriteMethodType=boolcRIO_Trig4NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig4;0;ReadMethodType=boolcRIO_Trig5NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig5;0;ReadMethodType=boolcRIO_Trig6NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig6;0;ReadMethodType=boolcRIO_Trig7NumberOfSyncRegistersForReadInProject=Auto;resource=/cRIO_Trig/cRIO_Trig7;0;ReadMethodType=boolcRIO-9056/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9056FPGA_TARGET_FAMILYARTIX7TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]Mod8/DIO0resource=/crio_Mod8/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO10resource=/crio_Mod8/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO11resource=/crio_Mod8/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO12resource=/crio_Mod8/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO13resource=/crio_Mod8/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO14resource=/crio_Mod8/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO15:8resource=/crio_Mod8/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO15resource=/crio_Mod8/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO16resource=/crio_Mod8/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO17resource=/crio_Mod8/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO18resource=/crio_Mod8/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO19resource=/crio_Mod8/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO1resource=/crio_Mod8/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO20resource=/crio_Mod8/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO21resource=/crio_Mod8/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO22resource=/crio_Mod8/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO23:16resource=/crio_Mod8/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO23resource=/crio_Mod8/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO24resource=/crio_Mod8/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO25resource=/crio_Mod8/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO26resource=/crio_Mod8/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO27resource=/crio_Mod8/DIO27;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO28resource=/crio_Mod8/DIO28;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO29resource=/crio_Mod8/DIO29;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO2resource=/crio_Mod8/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO30resource=/crio_Mod8/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO31:0resource=/crio_Mod8/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod8/DIO31:24resource=/crio_Mod8/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO31resource=/crio_Mod8/DIO31;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO3resource=/crio_Mod8/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO4resource=/crio_Mod8/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO5resource=/crio_Mod8/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO6resource=/crio_Mod8/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO7:0resource=/crio_Mod8/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod8/DIO7resource=/crio_Mod8/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO8resource=/crio_Mod8/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod8/DIO9resource=/crio_Mod8/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod8[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 8,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Offset from Time Reference ValidNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference Valid;0;ReadMethodType=boolOffset from Time ReferenceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Offset from Time Reference;0;ReadMethodType=i32Reset RT Appresource=/Reset RT App;0;WriteMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolSystem Watchdog ExpiredNumberOfSyncRegistersForReadInProject=Auto;resource=/System Watchdog Expired;0;ReadMethodType=boolTime SourceNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Source;0;ReadMethodType=Targets\NI\FPGA\RIO\CompactRIO\Sync\SyncSource.ctlTime Synchronization FaultNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time Synchronization Fault;0;ReadMethodType=boolTimeNumberOfSyncRegistersForReadInProject=0;resource=/Time Synchronization/Time;0;ReadMethodType=u64USER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolValues"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Arbitrate if Multiple Requestors Only;ElementsPerWrite=1;Implementation=2;Values;DataType=1000800000000001000A402104426F6F6C00000100000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="niFPGA Boolean Crossing.vi" Type="VI" URL="/&lt;vilib&gt;/express/rvi/analysis/control/nonlinear/niFPGA Boolean Crossing.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="FPGA" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">FPGA</Property>
						<Property Name="Comp.BitfileName" Type="Str">PWM-FGPA_FPGATarget_FPGA_noLuArdA6IE.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/C/Users/m.schmidt/Desktop/Projekte/FPGA Bitfiles/PWM-FGPA_FPGATarget_FPGA_noLuArdA6IE.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/PWM-FGPA_FPGATarget_FPGA_noLuArdA6IE.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/C/Users/m.schmidt/Desktop/Projekte/PWM-FGPA.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO-9056-01DEC88F/Chassis/FPGA Target/FPGA.vi</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="DrawGraph.vi" Type="VI" URL="../VI´s/DrawGraph.vi"/>
		<Item Name="GenerateBoolArray.vi" Type="VI" URL="../VI´s/GenerateBoolArray.vi"/>
		<Item Name="Reading.vi" Type="VI" URL="../VI´s/Reading.vi"/>
		<Item Name="RT.vi" Type="VI" URL="../RT/RT.vi"/>
		<Item Name="RTInit.vi" Type="VI" URL="../RT/RTInit.vi"/>
		<Item Name="RTProcessData.ctl" Type="VI" URL="../TypeDefs/RTProcessData.ctl"/>
		<Item Name="RTState.ctl" Type="VI" URL="../TypeDefs/RTState.ctl"/>
		<Item Name="Saved.ctl" Type="VI" URL="../TypeDefs/Saved.ctl"/>
		<Item Name="ScalingArray.vi" Type="VI" URL="../VI´s/ScalingArray.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Boolean Array to Digital.vi"/>
				<Item Name="Create Timing Source.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/TimedLoop/scheduler/LVUserAPI/Create Timing Source.vi"/>
				<Item Name="Create_1kHz_TSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/TimedLoop/scheduler/LVUserAPI/_suppVIs/Create_1kHz_TSource.vi"/>
				<Item Name="Create_1MHz_TSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/TimedLoop/scheduler/LVUserAPI/_suppVIs/Create_1MHz_TSource.vi"/>
				<Item Name="Create_SoftwareTrigger_TSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/TimedLoop/scheduler/LVUserAPI/_suppVIs/Create_SoftwareTrigger_TSource.vi"/>
				<Item Name="DTbl Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Boolean Array to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DWDT Boolean Array to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Boolean Array to Digital.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="lvalarms.dll" Type="Document" URL="lvalarms.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
